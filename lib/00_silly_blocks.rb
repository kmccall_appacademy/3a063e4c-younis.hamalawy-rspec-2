def reverser
  array_of_str = yield.split
  array_of_str.map! { |word| word.reverse }
  array_of_str.join' '
end

def adder(second_num = 1)
  yield + second_num
end

def repeater(n = 1)
  n.times { yield }
end
