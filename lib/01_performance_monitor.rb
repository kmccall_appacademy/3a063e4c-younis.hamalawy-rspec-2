def measure(n = 1)
  start = Time.now
  n.times { yield }
  finish = Time.now
  elapsed_time = finish - start
  average_time = elapsed_time / n
end
